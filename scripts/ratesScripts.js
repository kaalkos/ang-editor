$(document).click(function(e) {
// watch classes of the clicked element and close opened tooltip when user will click outside of it

  if($(e.target).attr('class') == undefined)
  {
  	 		$('.toolTiptext').css('visibility','hidden');
  }
  else{
		 if(!($(e.target).attr('class').includes('toolTipInput') || $(e.target).attr('class').includes('toolTiptext'))) 
		 {
		    $('.toolTiptext').css('visibility','hidden');
		  }
	}
});
