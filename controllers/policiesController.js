app.controller('policiesController',['$scope','$location','$rootScope','$http',function($scope,$location,$rootScope,$http){

    // id will be received from API in the future
    var hotelID = 1111;

    //connect with DB and fetch data if exit
    fetchChildrenPolicy();
    fetchDepositPolicy();
    fetchCancellationPolicy();


    //INITIALIZE JSON OBJECT FOR EACH POLICY
      var CommonJsonToSend = {};
      // first cancellation policy added by default
      $scope.cancellations = [];
      // $scope.cancellations.push(
      // {
      // "id": hotelID,
      // "name":"Non refundable",
      // "created": new Date(),
      // "updated": new Date(),
      // "rule":[{"daysBefore":"999","fee":"100","type":"%"}]
      // });
      

      $scope.children = {};
      $scope.children.policies = [];
      $scope.depositPolicies = [];

    //
    // DEPOSITS
    //
    $scope.deposits = false;
    $scope.depositAdditionalPayment = false;
    $scope.depositAdditionalPayment1 = false;

    $scope.deposit = {};
    $scope.deposit.amount = 30;
    $scope.deposit.daysBefore = 0;
    $scope.deposit.toDelete;

    var ids = 
    {
        deposit: hotelID,
        cancellation: hotelID,
        children: hotelID
    };


    $scope.toggleDeposits = function(range){
        if(range == "all")
        {
            $scope.deposits = !$scope.deposits;
            if($scope.depositAdditionalPayment1){
                $scope.depositAdditionalPayment1 = false;
            }

        }

        else if(range == "additional"){$scope.depositAdditionalPayment = !$scope.depositAdditionalPayment;}

        else if(range == "additional1"){$scope.depositAdditionalPayment1 = !$scope.depositAdditionalPayment1;}
    };

    $scope.editDeposit = function(depositEdited){
        $scope.editDep =  depositEdited + 1;
        $scope.deposit.policyName = $scope.depositPolicies[depositEdited].name;
        $scope.deposit.amount = $scope.depositPolicies[depositEdited].amount;
       
        //first additional payment
        if($scope.depositPolicies[depositEdited].additionalPayment == undefined){
            $scope.depositPolicies[depositEdited].additionalPayment = false;
        }

        if($scope.depositPolicies[depositEdited].additionalPayment.daysBefore && $scope.depositPolicies[depositEdited].additionalPayment){
            $scope.deposit.daysBefore = $scope.depositPolicies[depositEdited].additionalPayment.daysBefore;
            $scope.deposit.additionalPaymentAmount = $scope.depositPolicies[depositEdited].additionalPayment.amount;

               $scope.depositAdditionalPayment = true;

             //second additional payment
            if($scope.depositPolicies[depositEdited].additionalPayment1 == undefined){$scope.depositPolicies[depositEdited].additionalPayment1 = false}

            if($scope.depositPolicies[depositEdited].additionalPayment1.daysBefore && $scope.depositPolicies[depositEdited].additionalPayment1){
            $scope.deposit.daysBefore1 = $scope.depositPolicies[depositEdited].additionalPayment1.daysBefore;
            $scope.deposit.additionalPaymentAmount1 = $scope.depositPolicies[depositEdited].additionalPayment1.amount;

                $scope.depositAdditionalPayment1 = true;
            }
        }
    
        $scope.toggleDeposits('all');

    }

    $scope.addDeposit = function(instruction){
        
        //Check if name already exists 
        if(ifNameRepeated('deposit',$scope.deposit.policyName, instruction))
        {
            return alert('this policy name already exists');
        }

                  //validation    
                if(!(depositValidation($scope.deposit)))
                {
                    return alert('plese type your deposit form properly');
                }
      
                var currentTime = new Date();

                var oneDepositPolicy = 
                    {
                        "name": $scope.deposit.policyName,
                        "amount": $scope.deposit.amount
                    };

                //assign ID to each new object
                if(!instruction)
                {   
                    oneDepositPolicy.id =  ids.deposit;
                }
                else
                {   
                     oneDepositPolicy.id = $scope.depositPolicies[$scope.editDep-1].id;
                }

                // check if just created or only updated
                if(!instruction)
                {   
                    oneDepositPolicy.created = currentTime;
                    oneDepositPolicy.updated = currentTime;
                }
                else if(instruction)
                {
                    oneDepositPolicy.created = $scope.depositPolicies[$scope.editDep-1].created; 
                    oneDepositPolicy.updated = currentTime;
                }
                
                    if($scope.depositAdditionalPayment){   
                        oneDepositPolicy.additionalPayment = {};
                        oneDepositPolicy.additionalPayment.daysBefore = $scope.deposit.daysBefore;
                        oneDepositPolicy.additionalPayment.amount = $scope.deposit.additionalPaymentAmount;

                        if($scope.depositAdditionalPayment1)
                        {
                            oneDepositPolicy.additionalPayment1 = {};
                            oneDepositPolicy.additionalPayment1.daysBefore = $scope.deposit.daysBefore1;
                            oneDepositPolicy.additionalPayment1.amount = $scope.deposit.additionalPaymentAmount1;
                        }
                    }
               
               //allow to update record in database finding it by old name 
              if(instruction)
              {
                var oldName = $scope.depositPolicies[$scope.editDep-1].name;
              }

              //  instruction ? $scope.depositPolicies[$scope.editDep-1] = oneDepositPolicy : $scope.depositPolicies.push(oneDepositPolicy);
                 
            // send policy to database or update

            if(!instruction)
                {
                    $http.post('databaseSend/deposit', oneDepositPolicy)
                    .then(function(response){
                        $scope.depositPolicies.push(oneDepositPolicy);
                    })
                    .catch(function(err){
                       
                         $('#databaseError').modal('show');
                    });
                } 
            else if(instruction)
            {       
                    oneDepositPolicy.oldName = oldName;

                    $http.post('databaseUpdate/deposit', oneDepositPolicy)
                    .then(function(response){
                       $scope.depositPolicies[$scope.editDep-1] = oneDepositPolicy; 
                    })
                    .catch(function(err){
                        
                         $('#databaseError').modal('show');
                    });
            }

            $scope.cancelDeposit();
    };

    $scope.cancelDeposit = function(){
            $scope.editDep = null; 
            $scope.deposit.policyName = '';
            $scope.deposit.amount = 30;
            $scope.deposit.daysBefore = 0;
            $scope.deposit.daysBefore1 = 0;
            $scope.deposit.additionalPaymentAmount = 0;
            if($scope.depositAdditionalPayment){$scope.depositAdditionalPayment = !$scope.depositAdditionalPayment;}

            $scope.toggleDeposits('all');
    };

    $scope.deleteDeposit = function(number){
       
        $http.post('/databaseDelete/deposit',{deposit: $scope.depositPolicies[number], ratePlan: $scope.relatedRatePlan})
        .then(function(response){
                   $scope.depositPolicies.splice(number,1);
                })
        .catch(function(err){
                     $('#databaseError').modal('show');
                });

        
    };

    $scope.findDepositArray = function(indexOfArray){

         $scope.relatedRatePlan = {};
         $scope.relatedRatePlan.id = [];
         $scope.relatedRatePlan.name = [];

        $http.post('databaseDelete/depositFindRelated',$scope.depositPolicies[indexOfArray])
        .then(function(response){

            for(var i in response.data)
            {
                $scope.relatedRatePlan.id.push(response.data[i].id);
                $scope.relatedRatePlan.name.push(response.data[i].name);
            }
            
        })
        .catch(function(err){
            if(err){}
        });
       

        $scope.deposit.toDelete = indexOfArray;
    };
    //
    // CANCELLATIONS
    //

    $scope.cancellation = false;
    $scope.countRules = 1;
    $scope.cancellationForm = {};
    $scope.cancellationForm.toDelete = null;
    $scope.editedCancellation = null;

    $scope.toggleCancellations = function(){
       $scope.editedCancellation = false;
        $scope.cancellation = !$scope.cancellation;
        $scope.countRules = 1;
        $scope.cancellationForm = {};
    };

    $scope.addRule = function(){
        $scope.countRules++;
    };

    $scope.getRulesNumber = function(num){
        return new Array(num);
    };

    $scope.editCancellation = function(editedCanc){
        $scope.cancellation = true;
        $scope.editedCancellation = editedCanc;
        $scope.cancellationForm.policyName = $scope.cancellations[editedCanc].name;
        $scope.cancellationForm.oldName =  $scope.cancellationForm.policyName;
        

        $scope.countRules = Object.keys($scope.cancellations[editedCanc].rule).length
            
            // clean models for edit form 
            $scope.cancellationForm.period = [];
            $scope.cancellationForm.amount = [];
            $scope.cancellationForm.type = [];
            $scope.cancellationForm.dataBaseID = [];

        for(var i = 0; i < $scope.countRules; i++){
    
            $scope.cancellationForm.period[i] = $scope.cancellations[editedCanc].rule[i].daysBefore;
            $scope.cancellationForm.amount[i] = $scope.cancellations[editedCanc].rule[i].fee;
            $scope.cancellationForm.type[i] =  $scope.cancellations[editedCanc].rule[i].type;

            $scope.cancellationForm.dataBaseID[i] = $scope.cancellations[editedCanc].rule[i].id;
        }
    };

    $scope.deleteCancellation = function(number){

        $http.post('databaseDelete/cancellation', {cancellation: $scope.cancellations[number], ratePlans: $scope.relatedRatePlan})
        .then(function(response){
             $scope.cancellations.splice(number,1);
        })
        .catch(function(err){
            $('#databaseError').modal('show');
        });
      

    };

    $scope.saveCancellation = function(instruction){
        //Check if name already exists 
        if(ifNameRepeated('cancellation',$scope.cancellationForm.policyName,instruction))
        {
            return alert('this policy name already exists');
        }
        var currentTime = new Date();

        //validation
         if(!cancellationValidation($scope.cancellationForm))
        {
            return alert('please type your cancellation policy properly');
        }

        var oneCancellation = {};

        oneCancellation.name = $scope.cancellationForm.policyName;

        if(!instruction)
        {   
          oneCancellation.id =  ids.cancellation;
        }
        else
        {  
          oneCancellation.id = $scope.cancellations[$scope.editedCancellation].id;
        }

        // check if just created or only updated
        if(!instruction)
        {   
            oneCancellation.created = currentTime;
            oneCancellation.updated = currentTime;
        }
        else if(instruction)
        {   
            oneCancellation.created = $scope.cancellations[$scope.editedCancellation].created;
            oneCancellation.updated = currentTime;   
        }
       
        oneCancellation.rule = [];
    
        for(var i = 0; i < (Object.keys($scope.cancellationForm.period).length); i++){
            
            var rule = {};
            rule.daysBefore = $scope.cancellationForm.period[i];
            rule.fee = $scope.cancellationForm.amount[i];
            rule.type = $scope.cancellationForm.type[i];
            if(instruction)
            {
                rule.id = $scope.cancellationForm.dataBaseID[i];
            }
            oneCancellation.rule.push(rule);
        };

        // instruction ? $scope.cancellations[$scope.editedCancellation] = oneCancellation :  $scope.cancellations.push(oneCancellation);

        if(!instruction){
            $http.post('databaseSend/cancellation', oneCancellation)
            .then(function(response){

                $scope.cancellations.push(oneCancellation)

                // restart cancellation policy in order to assign ID to cancellation rules
                fetchCancellationPolicy();
            })
            .catch(function(err){
                 $('#databaseError').modal('show');
            });
        }
        else if(instruction)
        {   
            oneCancellation.oldName = $scope.cancellationForm.oldName;
            $http.post('databaseUpdate/cancellation', oneCancellation)
            .then(function(response){

                $scope.cancellations[$scope.editedCancellation] = oneCancellation

                // restart cancellation policy in order to assign ID to cancellation rules
                fetchCancellationPolicy();
            })
            .catch(function(err){
                $('#databaseError').modal('show');
            });
        }

        $scope.toggleCancellations();

        };

        $scope.findCancellationArray = function(indexOfArray)
        {

         //connect do database in order to check any related rate plans to delete 
         $scope.relatedRatePlan = {};
         $scope.relatedRatePlan.id = [];
         $scope.relatedRatePlan.name = [];

        $http.post('databaseDelete/cancellationFindRelated',$scope.cancellations[indexOfArray])
        .then(function(response){

            for(var i in response.data)
            {
                $scope.relatedRatePlan.id.push(response.data[i].id);
                $scope.relatedRatePlan.name.push(response.data[i].name);
            }
            
        })
        .catch(function(err){
            if(err){}
        });

        

        $scope.cancellationForm.toDelete = indexOfArray;

        }

    //
    //CHILDREN
    //

     $scope.kids = false;
     $scope.childrenForm = {};
     $scope.childrenForm.selectedAge = 0;
     $scope.childrenForm.selectedMinAgeRange = 0;

     $scope.watchChildAge = function(){
        if($scope.childrenForm.selectedAge == "none"){$scope.childrenForm.selectedMinAgeRange = 0}
        else{$scope.childrenForm.selectedMinAgeRange =  $scope.childrenForm.selectedAge};
     };

     $scope.addChildren = function(ifChildren){
        if(!ifChildren)
        {
             $scope.kids = false;
             delete $scope.children.policies;
             $scope.children.ifChildren = false;
             $('.yesChildren').removeClass('clickedRooms');
             $('.noChildren').addClass('clickedRooms');
        }
        else
        {   
            $scope.children.policies = [];
            $scope.children.ifChildren = true;
            $scope.kids = true;
            $('.noChildren').removeClass('clickedRooms');
            $('.yesChildren').addClass('clickedRooms');
        };
      };

      $scope.saveChildrenPolicy = function(instruction){
         //Validate and Check if name already exists 

         if(!("name" in $scope.childrenForm))
        {
            return alert('please type your children policy properly')
        }

        if($scope.childrenForm.name.length == 0)
        {
            return alert('please type your children policy properly');
        }
        
        if(ifNameRepeated('children',$scope.childrenForm.name,instruction))
        {
            return alert('this policy name already exists');
        }

          //check if save or edit
        // if(instruction)
        // {
        //     $scope.children.policies.splice($scope.childrenForm.policyToEdit,1);
        //     $scope.childrenForm.ifEdit = false;
        // };

        var currentTime = new Date();

        if(!$scope.children.ifChildren){
            delete $scope.children.ifChildren;
        };

        var oneChildrenPolicy = {};

         if(!instruction)
        {
            oneChildrenPolicy.created = currentTime;
            oneChildrenPolicy.updated = currentTime;
        }
        else if(instruction)
        {
            oneChildrenPolicy.created = $scope.children.policies[$scope.childrenForm.policyToEdit].created;
            oneChildrenPolicy.updated = currentTime;   
        }

        //catching old name in order to find it in database
        if(instruction)
        {
        var oldName = $scope.children.policies[$scope.childrenForm.policyToEdit].name;
        }

        oneChildrenPolicy.name = $scope.childrenForm.name;
        oneChildrenPolicy.ageRange = $('#ageLimits').val();

        //assign id
        if(!instruction)
        {
            oneChildrenPolicy.id = ids.children;
        }
        else
        {
           oneChildrenPolicy.id = $scope.children.policies[$scope.childrenForm.policyToEdit].id;
        }


        // instruction ? $scope.children.policies[$scope.childrenForm.policyToEdit] = oneChildrenPolicy :  $scope.children.policies.push(oneChildrenPolicy);

        //save or upload to database
         if(!instruction)
         {
            $http.post('databaseSend/children',oneChildrenPolicy)
            .then(function(response){

                $scope.children.policies.push(oneChildrenPolicy);
            })
            .catch(function(err){
                 $('#databaseError').modal('show');
            });
        }
        else if(instruction)
        {   
            oneChildrenPolicy.oldName = oldName;

            $http.post('databaseUpdate/children', oneChildrenPolicy)
            .then(function(response){

                $scope.children.policies[$scope.childrenForm.policyToEdit] = oneChildrenPolicy;
            })
            .catch(function(err){
                $('#databaseError').modal('show');
            });
        }

        //clean and close child policy form
        $scope.cancelChildPolicy();

      };

      $scope.cancelChildPolicy = function(){
        // hide children policy form and reset values
        $('.childrenCollapse').collapse('hide');
        // $('#ageLimits').val("0 - 17");
        $scope.childrenForm.name = null;
        $scope.childrenForm.ifEdit = false;
      };

      $scope.deleteChildPolicy = function(policyNumber){

    
        $http.post('databaseDelete/children', $scope.children.policies[policyNumber])
        .then(function(response){
            // delete children policy from local system
            $scope.children.policies.splice(policyNumber,1);
        })
        .catch(function(err){
            if(err){ return  $('#databaseError').modal('show');}
        });

       
      };

      $scope.editChildPolicy = function(policyNumber){
        $scope.childrenForm.ifEdit = true;

        $scope.childrenForm.policyToEdit = policyNumber;
        $('.childrenCollapse').collapse('show');
        $scope.childrenForm.name = $scope.children.policies[policyNumber].name;

        // update age limit in form with value from edited policy
        $('#ageLimits').val($scope.children.policies[policyNumber].ageRange);
      };

      
      //
      // COMMON FUNCTIONS FOR VALIDATION AND SENDING
      //

      $scope.sendData = function()
      {

        // Check if all policies are not empty
        if($scope.children.ifChildren == undefined || $scope.depositPolicies.length == 0){
            return alert('please fullfill all policies');
        }

        if($scope.children.ifChildren == true)
        {
            delete $scope.children.ifChildren;
        }

        CommonJsonToSend.deposit =  $scope.depositPolicies;
        CommonJsonToSend.cancellation =  $scope.cancellations;
        CommonJsonToSend.children =  $scope.children;
       
      }

      function ifNameRepeated(data,name,edited){
        // reject call if policy is edited
        if(edited){return false};

        switch (data){
            case 'deposit':
                for(var i = 0; i < $scope.depositPolicies.length; i++){
                    if($scope.depositPolicies[i].name == name){
                        return true;
                    }
                };
                break;
            case 'cancellation':

                for(var i = 0; i < $scope.cancellations.length; i++){
                    if($scope.cancellations[i].name == name){
                        return true;
                    }
                };
                break;
            case 'children':
                for(var i = 0; i < $scope.children.policies.length; i++){
                    if($scope.children.policies[i].name == name){
                        return true;
                     }
                };
                break;
            }
            return false;
      } 

      // validation
      function depositValidation(depositPolicy)
      {
        if(depositPolicy.policyName.length == 0){
            return false;
        }
        
        if(typeof(depositPolicy.additionalPayment) != 'undefined')
        {   
          
            if(depositPolicy.additionalPayment.amount <= 0){
                return false;
            }
            if(depositPolicy.additionalPayment.daysBefore <= 0){
                return false;
            }
        }

         if(typeof(depositPolicy.additionalPayment1) != 'undefined')
        {
            if(depositPolicy.additionalPayment1.amount <= 0){
                return false;
            }
            if(depositPolicy.additionalPayment1.daysBefore <= 0){
                return false;
            }
        }

            return true;
      }

      function cancellationValidation(cancellationPolicy)
      {
        if(typeof(cancellationPolicy.policyName) == 'undefined'){return false};

        if(cancellationPolicy.policyName.length == 0){return false};
        
        if(!("type" in cancellationPolicy) || !("amount" in cancellationPolicy) || !("period" in cancellationPolicy)){return false};

        for(var i = 0; i < $scope.countRules; i++)
        {   
            
            if(cancellationPolicy.amount[i] <= 0 || cancellationPolicy.period[i] <= 0 || typeof(cancellationPolicy.type[i]) == 'undefined' )
            {
                return false;
            }
        }

        //return true if object will pass validation process succesfully 
        return true;
      }

    //
    //connect with DB and find if user already has record
    //

    function fetchDepositPolicy()
    {
        $http.post('/user/deposit',{id: hotelID})
         .then(function(response){
            if(response.data != 'not found'){
                for(var i in response.data)
                {   
                    $scope.depositPolicies[i] = {};
                    $scope.depositPolicies[i].name = response.data[i].name;
                    $scope.depositPolicies[i].id = response.data[i].hotel_id;
                    $scope.depositPolicies[i].amount = response.data[i].deposit;
                    $scope.depositPolicies[i].additionalPayment = {} 
                    $scope.depositPolicies[i].additionalPayment.amount = response.data[i].add_amount;
                    $scope.depositPolicies[i].additionalPayment.daysBefore = response.data[i].add_days;
                    $scope.depositPolicies[i].additionalPayment1 = {} 
                    $scope.depositPolicies[i].additionalPayment1.amount = response.data[i].add_amount1;
                    $scope.depositPolicies[i].additionalPayment1.daysBefore = response.data[i].add_days1;
                }
            }

          }).catch(function(err){$('#databaseError').modal('show')});

    }
    
    function fetchChildrenPolicy()
    {
        $http.post('/user/children',{id: hotelID})
        .then(function(response){
            if(response.data != 'not found'){
                for(var i in response.data)
                {   
                    $scope.children.policies[i] = {};
                    $scope.children.policies[i].name = response.data[i].type;
                    $scope.children.policies[i].ageRange = response.data[i].age_range;
                    $scope.children.policies[i].id = response.data[i].hotel_id;
                }

                // mark a button if there are any policies in database
                if($scope.children.policies.length > 0)
                {   
                    $scope.children.ifChildren = true;
                    $('.yesChildren').addClass('clickedRooms');
                }
            }
         }).catch(function(err){ $('#databaseError').modal('show');});
    }
  

    function fetchCancellationPolicy()
    {
        $http.post('/user/cancellation',{id: hotelID})
            .then(function(response){
            if(response.data != 'not found'){
               
                for(var i in response.data.cancellations)
                {   
                    var id = response.data.cancellations[i].id;

                    $scope.cancellations[i] = {};
                    $scope.cancellations[i].rule = [];
                    $scope.cancellations[i].name = response.data.cancellations[i].name;
                    $scope.cancellations[i].id = response.data.cancellations[i].hotel_id;


                    for(var n in response.data.rules)
                    {
                        if(id == response.data.rules[n].cancellation_id)
                        {   
                            var temp = 
                            {
                                daysBefore: response.data.rules[n].period,
                                fee: response.data.rules[n].amount,
                                type: response.data.rules[n].type,
                                id: response.data.rules[n].id
                            }
                            $scope.cancellations[i].rule.push(temp);
                        }

                    }

                }
                   
            }   
         }).catch(function(err){$('#databaseError').modal('show');});

    }
      
}]);






