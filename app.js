var app = angular.module('onboarding',['ngRoute']);

app.config(function($routeProvider){
	$routeProvider
	.when('/policies',{
		templateUrl: 'views/policies.html',
		controller: 'policiesController'
	})
	.when('/ratePlans',{
		templateUrl: 'views/ratePlan.html',
		controller: 'policiesController'
	})
	.when('/rates',{
		templateUrl: 'views/rates.html',
		controller: 'ratesController'
	})
	 
});